package com.example.maybenow

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.system.Os.close
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.maybenow.data.User
import com.example.maybenow.data.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val CONTACT_PERMISSION_CODE = 1;
    var userId : Int = 7;
    //contact pick code
    private val CONTACT_PICK_CODE = 2
    private lateinit var mUserViewModel : UserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        button2.setOnClickListener {

            var dialog = CustomDialogFragment()

            dialog.show(supportFragmentManager, "customDialog")
        }
        button.setOnClickListener {


            getContent.launch(null)

            userId++

        }
    }
    @SuppressLint("Range")
    private val getContent = registerForActivityResult(ActivityResultContracts.PickContact()){uri ->
        var name = ""


         contentResolver?.query(uri,
            arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME), null, null, null)?.apply{
            moveToNext()
             name = getString(0)


             mUserViewModel.addUser(User(userId, name,"",0, "37529 $userId" ))


            close()
        }


    }






}

